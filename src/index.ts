#!/usr/bin/env node
import * as program from 'commander';
import * as shell from 'shelljs';
import 'colors';
import * as fs from 'fs-extra';

let appName = '';
let appDirectory = '';

function createReactApp(): Promise<boolean> {
  return new Promise((resolve) => {
    if (appName) {
      console.log('\nCreating react app...'.cyan);
      try {
        shell.exec(
          `npx create-react-app --template typescript ${appName}`,
          (e, _stdout, stderr) => {
            if (e !== 0) {
              console.log('E: ', e);
              console.log('Stderr: ', stderr);
              resolve(false);
            } else {
              console.log('Finished creating react app'.green);
              resolve(true);
            }
          },
        );
      } catch (e) {
        console.log('create-react-app not installed'.red);
        resolve(false);
        process.exit(1);
      }
    } else {
      console.log('\nNo app name was provided.'.red);
      console.log('\nProvide an app name in the following format: ');
      console.log('\nff-create-react-app ', 'app-name\n'.cyan);
      resolve(false);
      process.exit(1);
    }
  });
}
function installPackages(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInstalling packages...'.cyan);
    const command =
      'npm install' +
      ' react-router-dom' +
      ' axios' +
      ' aws-amplify' +
      ' aws-amplify-react' +
      ' react-jss' +
      ' @material-ui/core';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nFinished installing packages\n'.green);
      resolve();
    });
  });
}

function installDevPackages(): Promise<void> {
  return new Promise((resolve) => {
    console.log('\nInstalling dev packages...'.cyan);
    const command =
      'npm install --save-dev' +
      ' @types/react-router-dom' +
      ' typescript@latest' +
      ' @types/node' +
      ' @types/react' +
      ' @types/react-dom' +
      ' @types/jest' +
      ' @types/react-jss';
    shell.exec(command, { cwd: appDirectory }, () => {
      console.log('\nFinished installing dev packages\n'.green);
      resolve();
    });
  });
}

function configureAmplify(): void {
  if (require.main) {
    const appSrcFile = `${require('path').dirname(
      require.main.filename,
    )}/templates/aws-custom-exports.txt`;
    const amplifyFile = `${appDirectory}/src/aws-custom-exports.ts`;
    fs.copySync(appSrcFile, amplifyFile);
  }
}

function generateBoilerplate(): void {
  console.log('\nGenerating boilerplate...'.cyan);
  fs.unlinkSync(`${appDirectory}/src/App.css`);
  fs.unlinkSync(`${appDirectory}/src/logo.svg`);
  if (require.main) {
    const basePath = require('path').dirname(require.main.filename);
    const appSrcFile = `${basePath}/templates/App.txt`;
    const appFile = `${appDirectory}/src/App.tsx`;
    fs.copySync(appSrcFile, appFile);

    const avatarSrcFile = `${basePath}/templates/navigation/Avatar.txt`;
    const avatarFile = `${appDirectory}/src/navigation/Avatar.tsx`;
    fs.copySync(avatarSrcFile, avatarFile);

    const navBarSrcFile = `${basePath}/templates/navigation/NavBar.txt`;
    const navBarFile = `${appDirectory}/src/navigation/NavBar.tsx`;
    fs.copySync(navBarSrcFile, navBarFile);

    const navigationSrcFile = `${basePath}/templates/navigation/Navigation.txt`;
    const navigationFile = `${appDirectory}/src/navigation/Navigation.tsx`;
    fs.copySync(navigationSrcFile, navigationFile);

    const signOutSrcFile = `${basePath}/templates/navigation/SignOut.txt`;
    const signOutFile = `${appDirectory}/src/navigation/SignOut.tsx`;
    fs.copySync(signOutSrcFile, signOutFile);

    const colorsSrcFile = `${basePath}/templates/shared/colors.txt`;
    const colorsFile = `${appDirectory}/src/shared/colors.ts`;
    fs.copySync(colorsSrcFile, colorsFile);
  }
}

async function createReact(dir: string): Promise<void> {
  appName = dir;
  appDirectory = `${process.cwd()}/${appName}`;
  const cra = shell.which('create-react-app');
  if (!cra) {
    console.log(
      `create-react-app not installed \n install create-react-app first globally using :`
        .red,
    );
    console.log(`npm install -g create-react-app`.white);
    process.exit(1);
  }
  const success = await createReactApp();
  if (!success) {
    console.log(
      'Something went wrong while trying to create a new React app using create-react-app'
        .red,
    );
    process.exit(1);
  } else {
    await installPackages();
    await installDevPackages();
    configureAmplify();
    generateBoilerplate();
    shell.exec(`npm install`, { cwd: appDirectory }, () => {
      console.log('All done');
    });
  }
}

program
  .name('ff-create-react-app')
  .version('1.0.2')
  .command('init <dir>')
  .action(createReact);
program.parse(process.argv);
